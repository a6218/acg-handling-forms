import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ExerciseAComponent } from './components/exercise-a/exercise-a.component';
import { ExerciseBComponent } from './components/exercise-b/exercise-b.component';
import { ExerciseCComponent } from './components/exercise-c/exercise-c.component';
import { ExerciseDComponent } from './components/exercise-d/exercise-d.component';
import { ExerciseEComponent } from './components/exercise-e/exercise-e.component';
import { ExerciseFComponent } from './components/exercise-f/exercise-f.component';
import { ExerciseGComponent } from './components/exercise-g/exercise-g.component';
import { ExerciseHComponent } from './components/exercise-h/exercise-h.component';
import { ExerciseIComponent } from './components/exercise-i/exercise-i.component';
import { ExerciseJComponent } from './components/exercise-j/exercise-j.component';
import { ExerciseKComponent } from './components/exercise-k/exercise-k.component';

@NgModule({
  declarations: [
    AppComponent,
    ExerciseAComponent,
    ExerciseBComponent,
    ExerciseCComponent,
    ExerciseDComponent,
    ExerciseEComponent,
    ExerciseFComponent,
    ExerciseGComponent,
    ExerciseHComponent,
    ExerciseIComponent,
    ExerciseJComponent,
    ExerciseKComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
