import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseFComponent } from './exercise-f.component';

describe('ExerciseFComponent', () => {
  let component: ExerciseFComponent;
  let fixture: ComponentFixture<ExerciseFComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseFComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseFComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
