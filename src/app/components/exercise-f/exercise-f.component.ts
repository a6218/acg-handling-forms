import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GENDER, OPTIONS } from 'src/app/types/forms';

interface UserFields {
  username: string,
  email: string,
  secret: string,
  selectQuestion: string,
  genderSelect: string,
  genderRadio: string,
}

@Component({
  selector: 'app-exercise-f',
  templateUrl: './exercise-f.component.html',
  styleUrls: ['./exercise-f.component.scss']
})
export class ExerciseFComponent {
  readonly defaultOption = OPTIONS.PET;

  readonly defaultGender = GENDER.FEMALE;
  readonly genders = [GENDER.FEMALE, GENDER.MALE];

  submitted = false;
  secretAnswer = '';
  userFields: UserFields = {
    username: '',
    email: '',
    secret: '',
    selectQuestion: '',
    genderSelect: '',
    genderRadio: '',
  }

  onSubmit(form: NgForm): void {
    console.log('form: ', form)
    this.submitted = true;
    this.setUserData(form);
  }

  setUserData(form: NgForm): void {
    this.userFields = {
      username: form.value.userData.username,
      email: form.value.userData.email,
      secret: form.value.secret,
      selectQuestion: form.value.secretQuestion,
      genderSelect: form.value.genderSelect,
      genderRadio: form.value.genderRadio,
    }
  }

  onSetUsername(form: NgForm): void {
    console.log('form: ', form);
    form.form.patchValue({ userData: {
      username: 'Dude',
    }});
  }

  onClear(form: NgForm): void {
    this.submitted = false;
    form.reset();
  }
}
