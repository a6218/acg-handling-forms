import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OPTIONS } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-d',
  templateUrl: './exercise-d.component.html',
  styleUrls: ['./exercise-d.component.scss']
})

export class ExerciseDComponent {
  readonly defaultOption = OPTIONS.PET;
  secretAnswer = '';

  onSubmit(form: NgForm): void {
    console.log('form: ', form)
  }
}
