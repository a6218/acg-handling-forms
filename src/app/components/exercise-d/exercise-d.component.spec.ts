import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseDComponent } from './exercise-d.component';

describe('ExerciseDComponent', () => {
  let component: ExerciseDComponent;
  let fixture: ComponentFixture<ExerciseDComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseDComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
