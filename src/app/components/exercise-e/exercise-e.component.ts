import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { GENDER, OPTIONS } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-e',
  templateUrl: './exercise-e.component.html',
  styleUrls: ['./exercise-e.component.scss']
})

export class ExerciseEComponent {
  readonly defaultOption = OPTIONS.PET;
  secretAnswer = '';

  readonly defaultGender = GENDER.FEMALE;
  readonly genders = [GENDER.FEMALE, GENDER.MALE];

  onSubmit(form: NgForm): void {
    console.log('form: ', form)
  }
}
