import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseEComponent } from './exercise-e.component';

describe('ExerciseEComponent', () => {
  let component: ExerciseEComponent;
  let fixture: ComponentFixture<ExerciseEComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseEComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseEComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
