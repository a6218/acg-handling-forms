import { Component } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { GENDER } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-h',
  templateUrl: './exercise-h.component.html',
  styleUrls: ['./exercise-h.component.scss']
})
export class ExerciseHComponent {
  readonly genders = [GENDER.FEMALE, GENDER.MALE];
  signupForm: FormGroup;

  constructor() {
    this.signupForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'userData': new FormGroup({
        'email': new FormControl(null, [Validators.required, Validators.email]),
      }),
      'gender': new FormControl(GENDER.FEMALE),
      'hobbies': new FormArray([]),
    })
  }

  get hobbyControls() {
    return (this.signupForm.get('hobbies') as FormArray).controls;
  }

  onAddHobbies(): void {
    const hobby = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(hobby);
  }

  onSubmit(): void {
    console.log(this.signupForm);
  }

  onClear(): void {
    this.signupForm.reset();
  }
}
