import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseHComponent } from './exercise-h.component';

describe('ExerciseHComponent', () => {
  let component: ExerciseHComponent;
  let fixture: ComponentFixture<ExerciseHComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseHComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseHComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
