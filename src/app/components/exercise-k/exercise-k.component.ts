import { Component } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { PROJECTSTATUS } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-k',
  templateUrl: './exercise-k.component.html',
  styleUrls: ['./exercise-k.component.scss']
})
export class ExerciseKComponent {
  readonly projectStatus = [PROJECTSTATUS.STABLE, PROJECTSTATUS.CRITICAL, PROJECTSTATUS.FINISHED];
  projectForm: UntypedFormGroup;

  constructor(private fb: UntypedFormBuilder) {
    this.projectForm = this.fb.group({
      projectname: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      projectstatus: [PROJECTSTATUS.CRITICAL]
    });
  }

  onSubmit(): void {
    console.log('form: ', this.projectForm)
  }
}
