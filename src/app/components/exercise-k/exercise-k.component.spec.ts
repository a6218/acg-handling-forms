import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseKComponent } from './exercise-k.component';

describe('ExerciseKComponent', () => {
  let component: ExerciseKComponent;
  let fixture: ComponentFixture<ExerciseKComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseKComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
