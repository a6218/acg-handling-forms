import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { delay, map, Observable, of } from 'rxjs';
import { PROJECTSTATUS } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-j',
  templateUrl: './exercise-j.component.html',
  styleUrls: ['./exercise-j.component.scss']
})
export class ExerciseJComponent {
  readonly projectStatus = [PROJECTSTATUS.STABLE, PROJECTSTATUS.CRITICAL, PROJECTSTATUS.FINISHED];
  projectForm: FormGroup;
  private readonly forbiddenProjectNames = ['test'];
  private readonly forbiddenProjectAsyncNames = ['Best', 'rEst'];

  constructor() {
    this.projectForm = new FormGroup({
      projectname: new FormControl(null, [Validators.required, this.checkProjectNames()], [this.CheckProjectNamesAsync.bind(this)]),
      email: new FormControl(null, [Validators.required, Validators.email]),
      projectstatus: new FormControl(PROJECTSTATUS.CRITICAL),
    });
  }

  checkProjectNames(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const forbidden = this.forbiddenProjectNames.some(projectname => {
        if(control.value === null) {
          return false;
        }

        const projectnameLower = projectname.toLocaleLowerCase();
        const valueLower = control.value.toLocaleLowerCase();
        return projectnameLower === valueLower;
      });

      console.log(forbidden);
      return forbidden ? { forbiddenName: { value: control.value } } : null;
    }
  }

  CheckProjectNamesAsync(control: AbstractControl): Observable<ValidationErrors | null> {
    return of(this.forbiddenProjectAsyncNames.some(projectname => {
      const projectnameLower = projectname.toLocaleLowerCase();
      const valueLower = control.value.toLocaleLowerCase();
      return projectnameLower === valueLower;
    }))
    .pipe(
      delay(3000),
      map(result => result ? { forbiddenNames: { value: control.value } } : null),
    );
  }

  onSubmit(): void {
    console.log(this.projectForm);
  }
}
