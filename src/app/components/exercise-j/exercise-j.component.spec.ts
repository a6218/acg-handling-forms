import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseJComponent } from './exercise-j.component';

describe('ExerciseJComponent', () => {
  let component: ExerciseJComponent;
  let fixture: ComponentFixture<ExerciseJComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseJComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseJComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
