import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseCComponent } from './exercise-c.component';

describe('ExerciseCComponent', () => {
  let component: ExerciseCComponent;
  let fixture: ComponentFixture<ExerciseCComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseCComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseCComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
