import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OPTIONS } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-c',
  templateUrl: './exercise-c.component.html',
  styleUrls: ['./exercise-c.component.scss']
})
export class ExerciseCComponent {
  readonly defaultOption = OPTIONS.PET;
  secretAnswer = '';

  onSubmit(form: NgForm) {
    console.log('form: ', form);
  }
}
