import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseIComponent } from './exercise-i.component';

describe('ExerciseIComponent', () => {
  let component: ExerciseIComponent;
  let fixture: ComponentFixture<ExerciseIComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseIComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseIComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
