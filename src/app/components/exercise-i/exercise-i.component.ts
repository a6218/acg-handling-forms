import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { delay, map, Observable, of, Subject, takeUntil } from 'rxjs';
import { GENDER } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-i',
  templateUrl: './exercise-i.component.html',
  styleUrls: ['./exercise-i.component.scss']
})
export class ExerciseIComponent implements OnInit {
  readonly genders = [GENDER.FEMALE, GENDER.MALE];
  signupForm: FormGroup;
  private readonly forbiddenNames = ['Jim', 'Tim'];
  private readonly forbiddenEmails = ['test@web.de', 'test@freenet.de'];
  private onDestroy = new Subject();

  constructor() {
    this.signupForm = new FormGroup({
      'username': new FormControl(null, [Validators.required, this.checkForbiddenNames()]),
      'userData': new FormGroup({
        'email': new FormControl(null, [Validators.required, Validators.email], this.checkForbiddenEmails.bind(this)),
      }),
      'gender': new FormControl(GENDER.FEMALE),
      'hobbies': new FormArray([]),
    });
  }

  ngOnInit(): void {
    this.signupForm.valueChanges
    .pipe(
      takeUntil(this.onDestroy),
    )
    .subscribe(res => console.log('val-res: ', res));

    this.signupForm.statusChanges
    .pipe(
      takeUntil(this.onDestroy),
    ).subscribe(res => console.log('status-res: ', res));

    this.signupForm.setValue({
      'username': 'Kim',
      'userData': {
        'email': 'kim@tim.de',
      },
      'gender': GENDER.MALE,
      'hobbies': [],
    });

    this.signupForm.patchValue({
      'username': 'Mark',
    });
  }

  get hobbyControls() {
    return (this.signupForm.get('hobbies') as FormArray).controls;
  }

  get nameForbidden(): boolean | null {
    const errors = this.signupForm.get('username')?.errors;
    return errors !== null && 'forbiddenName' in errors! ? true : false;
  }

  get nameRequired(): boolean | null {
    const errors = this.signupForm.get('username')?.errors;
    const touched = this.signupForm.get('username')?.touched;
    return errors !== null && touched && 'required' in errors! ? true : false;
  }

  onAddHobbies(): void {
    const hobby = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies')).push(hobby);
  }

  onSubmit(): void {
    console.log(this.signupForm);
  }

  onClear(): void {
    this.signupForm.reset();
  }

  checkForbiddenNames(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const forbidden = this.forbiddenNames.some(item => {
        if (control.value === null) {
          return false;
        }
        const inputLower = (control.value as string).toLocaleLowerCase();
        const itemLower = (item as string).toLocaleLowerCase();

        return itemLower === inputLower;
      });

      return forbidden ? {forbiddenName: {value: control.value}} : null;
    }
  }

  checkForbiddenEmails(control: AbstractControl): Observable<ValidationErrors | null> {
    return of(this.forbiddenEmails.some(email => {
      const emailLower = email.toLocaleLowerCase();
      const valueLower = control.value.toLocaleLowerCase();
      return email === control.value;
    })).pipe(
      delay(3000),
      map((result: boolean) => result ? { forbiddenEmail: { value: control.value } } : null),
    );
  }
}
