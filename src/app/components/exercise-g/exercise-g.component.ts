import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SUBSCRIPTIONS } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-g',
  templateUrl: './exercise-g.component.html',
  styleUrls: ['./exercise-g.component.scss']
})

export class ExerciseGComponent {
  readonly subscriptionOptions = [
    SUBSCRIPTIONS.BASIC, SUBSCRIPTIONS.ADVANCED, SUBSCRIPTIONS.PRO
  ]
  readonly defaultSubscription = SUBSCRIPTIONS.ADVANCED;

  onSubmit(form: NgForm): void {
    console.log('form: ', form.value);
  }
}
