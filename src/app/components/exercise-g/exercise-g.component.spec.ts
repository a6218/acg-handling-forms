import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseGComponent } from './exercise-g.component';

describe('ExerciseGComponent', () => {
  let component: ExerciseGComponent;
  let fixture: ComponentFixture<ExerciseGComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseGComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
