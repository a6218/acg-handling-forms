import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseBComponent } from './exercise-b.component';

describe('ExerciseBComponent', () => {
  let component: ExerciseBComponent;
  let fixture: ComponentFixture<ExerciseBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseBComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
