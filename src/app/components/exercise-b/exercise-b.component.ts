import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OPTIONS } from 'src/app/types/forms';

@Component({
  selector: 'app-exercise-b',
  templateUrl: './exercise-b.component.html',
  styleUrls: ['./exercise-b.component.scss']
})
export class ExerciseBComponent {
  readonly defaultOption = OPTIONS.PET;

  onSubmit(form: NgForm): void {
    console.log('form: ', form);
  }
}
