import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseAComponent } from './exercise-a.component';

describe('ExerciseAComponent', () => {
  let component: ExerciseAComponent;
  let fixture: ComponentFixture<ExerciseAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseAComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
