import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-exercise-a',
  templateUrl: './exercise-a.component.html',
  styleUrls: ['./exercise-a.component.scss']
})

export class ExerciseAComponent {
  onSubmit(form: NgForm) {
    console.log('form: ', form);
  }
}
