export enum OPTIONS {
  PET = 'pet',
  TEACHER = 'teacher',
}

export enum GENDER {
  FEMALE = 'female',
  MALE = 'male',
}

export enum SUBSCRIPTIONS {
  BASIC = 'basic',
  ADVANCED = 'advanced',
  PRO = 'pro',
}

export enum PROJECTSTATUS {
  STABLE = 'stable',
  CRITICAL = 'critical',
  FINISHED = 'finished',
}
